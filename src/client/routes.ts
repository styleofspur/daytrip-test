import App           from './containers/app/app.component';
import AppRoutesEnum from './utils/AppRoutes.enum';

export default {
    component: App,
    path:      AppRoutesEnum.ORDER,
    indexRoute: {
        component: require('./pages/order/order.component.tsx')
    },
    childRoutes: getRoutesPaths()
};

function getRoutesPaths() {
    let initialized = [];

    [
        'order',
        'checkout'

    ].forEach(path => {
        let fileName;
        if (path.includes('/')) {
            const parts = path.split('/');
            fileName    = parts[parts.length - 1];
        } else {
            fileName = path;
        }

        initialized.push(require(`./pages/${path}/${fileName}.route`));
    });

    return initialized;
}