import * as React           from 'react';
import { observer, inject } from 'mobx-react';

@inject('routingStore')
@observer
export default class App extends React.Component<{children: any, params: any, routingStore: any}, {}> {

    /**
     *
     * @public
     * @returns {JSX.Element}
     */
    public render(): JSX.Element {
        return (
            <div className="app">
                { this.props.children }
            </div>
        );
    }
}
