interface FetchedProduct {
    id:    string;
    price: number;
    count: number
}

export default FetchedProduct;