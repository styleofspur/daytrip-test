/// <reference path="../../../../node_modules/@types/webpack-env/index.d.ts" />
import AppRoutesEnum from '../../utils/AppRoutes.enum';

export default {
    path: AppRoutesEnum.ORDER,
    getComponent(location, cb) {
        require.ensure([], require => {
            cb(null, require('./order.component.tsx'));
        }, 'order');
    }
};

if (module.hot) {
    module.hot.accept();
}
module.exports = exports.default;