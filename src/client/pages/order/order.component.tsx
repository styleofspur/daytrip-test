import * as React           from 'react';
import * as moment          from 'moment';
import { observer, inject } from 'mobx-react';
import { browserHistory }   from 'react-router';
import {
    Jumbotron,
    Form,
    FormGroup,
    FormControl,
    ControlLabel,
    Button,
    Row,
    Col,
} from 'react-bootstrap';
import * as MaskedInput    from 'react-bootstrap-maskedinput';
import * as DateTimePicker from 'react-datetime';

import { Select }             from '../../components/select/select.component';
import { ErrorMessages }      from '../../components/error-messages/error-messages.component';
import { OrderStore }         from '../../stores/order/OrderStore';
import { ProductsStore }      from '../../stores/products/ProductsStore';
import { DeliveryTypesStore } from '../../stores/delivery-types/DeliveryTypesStore';
import FetchedProduct         from './FetchedProduct.interface';
import AppRoutesEnum          from '../../utils/AppRoutes.enum';

@inject('orderStore', 'productsStore', 'deliveryTypesStore')
@observer
export default class OrderPage
    extends React.Component<{
        orderStore:         OrderStore,
        productsStore:      ProductsStore,
        deliveryTypesStore: DeliveryTypesStore
    }, {}>
{


    /**
     * @public
     * @constructor
     * @param props
     */
    public constructor(props) {
        super(props);

        this.onDeliveryDatePick = this.onDeliveryDatePick.bind(this);
        this.hideErrors         = this.hideErrors.bind(this);
        this.onChange           = this.onChange.bind(this);
        this.onSubmit           = this.onSubmit.bind(this);
    }

    /**
     *
     * @public
     * @returns {void}
     */
    public hideErrors(): void {
        this.props.orderStore.resetErrors();
    }

    /**
     *
     * @public
     * @returns {FetchedProduct[]}
     */
    public fetchProducts(): FetchedProduct[] {
        const added: Array<FetchedProduct> = [];
        Array.from(document.querySelectorAll('input.pizza-count')).forEach((input: HTMLInputElement) => {
            if (+input.value) {
                added.push({
                    id:    input.dataset['id'],
                    count: +input.value,
                    price: this.props.productsStore.getPrice(input.dataset['id'])
                })
            }
        });

        return added;
    }

    /**
     *
     * @public
     * @param {moment.Moment} value
     * @returns {void}
     */
    public onDeliveryDatePick(value: moment.Moment): void {
        this.onChange({
            target: {
                name: 'deliveryDate', value: value.toString()
            }
        });
    }

    /**
     *
     * @public
     * @param evt
     * @returns {void}
     */
    public onChange(evt: React.ChangeEvent<any>|{target: {name: string, value: any}}): void {
        const { name, value } = evt.target;

        if (name === 'products[]') {
            this.props.orderStore.addProducts(this.fetchProducts())
        } else {
            this.props.orderStore.order[name] = value;
        }

        if (name === 'deliveryType') {
            this.props.orderStore.updateTotalAmount();
        }
    }

    /**
     * @public
     * @param evt
     * @returns {void}
     */
    public onSubmit(evt: React.SyntheticEvent<any>): void {
        evt.preventDefault();

        this.hideErrors();

        this.props.orderStore.order.deliveryType    = evt.target['deliveryType'].value;
        this.props.orderStore.order.deliveryDate    = evt.target['deliveryDate'].value;
        this.props.orderStore.order.deliveryAddress = evt.target['deliveryAddress'].value;
        this.props.orderStore.order.contactDetails  = {
            name:        evt.target['contactDetails.name'].value,
            phoneNumber: evt.target['contactDetails.phoneNumber'].value,
        };
        this.props.orderStore.addProducts(this.fetchProducts());

        this.props.orderStore.validate();
        if (this.props.orderStore.hasErrors()) {
            window.scrollTo(0, 0); // show errors at the top of the page
        } else {
            this.props.orderStore.create()
                .then(() => browserHistory.push(AppRoutesEnum.CHECKOUT));
        }
    }

    /**
     *
     * @public
     * @returns {JSX.Element}
     */
    public render(): JSX.Element {
        return (
            <div className="page order">
                {
                    !this.props.productsStore.flags.isFetched ?
                        <div className="preloader"></div> : null
                }
                <div className="container" hidden={!this.props.productsStore.flags.isFetched}>
                    <h1 className="page-header">Order</h1>
                    {
                        this.props.orderStore.hasErrors() ?
                            <ErrorMessages
                                errors={this.props.orderStore.errors}
                                onClose={this.hideErrors}
                            /> : null
                    }
                    <Jumbotron>
                        <Form
                            name="orderForm"
                            onSubmit={this.onSubmit}
                            onChange={this.onChange}
                            noValidate
                            autoComplete="off"
                        >
                            <div className="order-items">
                                <h3>Products</h3>
                                <ProductsList
                                    list={this.props.productsStore.pizzas.concat(this.props.productsStore.beverages)}
                                    values={this.props.orderStore.order.products}
                                    className="pizza-count"
                                />
                            </div>
                            <div className="delivery">
                                <h3>Delivery</h3>
                                <Select
                                    name="deliveryType"
                                    label="Delivery Type"
                                    list={this.props.deliveryTypesStore.list}
                                    options={{
                                        cssClass:    'delivery-type-option',
                                        valueProp:   'id',
                                        displayProp: 'name',
                                        required:     true
                                    }}
                                    defaultValue={this.props.orderStore.order.deliveryType}
                                    emptyOptionText="Select delivery type..."
                                />
                                <FormGroup
                                    className="required"
                                    bsSize="large"
                                >
                                    <ControlLabel>Delivery Date</ControlLabel>
                                    <DateTimePicker
                                        inputProps={{
                                            name:         'deliveryDate',
                                            placeholder:  'Delivery Date',
                                            value:        moment(this.props.orderStore.order.deliveryDate).format('MM/DD/YYYY HH:mm')
                                        }}
                                        isValidDate={value => value.isSame(new Date(), 'day') || value.isAfter(new Date())}
                                        onChange={this.onDeliveryDatePick}
                                        closeOnSelect={true}
                                        defaultValue={moment(this.props.orderStore.order.deliveryDate).toDate()}
                                    />
                                </FormGroup>
                                <FormGroup
                                    className={
                                        this.props.deliveryTypesStore.isExpress(this.props.orderStore.order.deliveryType) ? 'required' : ''
                                    }
                                    bsSize="large"
                                >
                                    <ControlLabel>Delivery Address</ControlLabel>
                                    <FormControl
                                        componentClass="textarea"
                                        name="deliveryAddress"
                                        placeholder="Delivery Address"
                                        defaultValue={this.props.orderStore.order.deliveryAddress}
                                    />
                                </FormGroup>
                            </div>
                            <div className="contact-details">
                                <h3>Contact Details</h3>
                                <FormGroup
                                    className="required"
                                    bsSize="large"
                                >
                                    <ControlLabel>Name</ControlLabel>
                                    <FormControl
                                        type="text"
                                        placeholder="Name"
                                        name="contactDetails.name"
                                        defaultValue={this.props.orderStore.order.contactDetails.name}
                                    />
                                </FormGroup>
                                <FormGroup
                                    className="required"
                                    bsSize="large"
                                >
                                    <ControlLabel>Phone Number</ControlLabel>
                                    <MaskedInput
                                        name="contactDetails.phoneNumber"
                                        placeholder="Phone Number"
                                        mask='111-111-1111'
                                        value={this.props.orderStore.order.contactDetails.phoneNumber}
                                    />
                                </FormGroup>
                            </div>
                            <div>
                                <p>
                                    Products: { this.props.orderStore.productsAmount }
                                </p>
                                <p>
                                    Delivery: { this.props.deliveryTypesStore.getPrice(this.props.orderStore.order.deliveryType) }
                                </p>
                                <p>
                                    <strong>Total: </strong>{ this.props.orderStore.order.totalAmount }
                                </p>
                            </div>
                            <div className="text-center">
                                <Button
                                    type="submit"
                                    bsSize="large"
                                    bsStyle="primary"
                                >
                                    Checkout
                                </Button>
                            </div>
                        </Form>
                    </Jumbotron>
                </div>
            </div>
        );
    }

}

/**
 * @param {*} props
 * @returns {JSX.Element}
 */
const ProductsList = props => {
    return (
        <div className="products-list">
            {
                props.list.map((item, i) => {
                    let count: string  = '0';
                    const setItem: any = props.values.find(i => i.id === item.id);
                    if (setItem) {
                        count = setItem.count;
                    }

                    return (
                        <Row key={i}>
                            <Col xs={4} className="product-item-name">{item.name}</Col>
                            <Col xs={4} className="product-item-price">$ {item.price}</Col>
                            <Col xs={4} className="product-item-count">
                                <FormControl
                                    type="number"
                                    min="1"
                                    max="10"
                                    name="products[]"
                                    className={props.className}
                                    data-id={item.id}
                                    placeholder="Count"
                                    defaultValue={count}
                                />
                            </Col>
                        </Row>
                    );
                })
            }
        </div>
    );
};

module.exports = exports.default;