import * as React               from 'react';
import * as moment              from 'moment';
import { observer, inject }     from 'mobx-react';
import { Link, browserHistory } from 'react-router';
import {
    Jumbotron,
    Button,
    Row,
    Col
} from 'react-bootstrap';

import { ErrorMessages }      from '../../components/error-messages/error-messages.component';
import { OrderStore }         from '../../stores/order/OrderStore';
import { ProductsStore }      from '../../stores/products/ProductsStore';
import { DeliveryTypesStore } from '../../stores/delivery-types/DeliveryTypesStore';
import AppRoutesEnum          from '../../utils/AppRoutes.enum';

@inject('orderStore', 'productsStore', 'deliveryTypesStore')
@observer
export default class CheckoutPage
    extends React.Component<{
        orderStore:         OrderStore,
        productsStore:      ProductsStore,
        deliveryTypesStore: DeliveryTypesStore
    }, {}>
{

    /**
     * @public
     * @constructor
     * @param props
     */
    public constructor(props) {
        super(props);

        this.hideErrors = this.hideErrors.bind(this);
        this.onPay      = this.onPay.bind(this);
    }

    /**
     *
     * @public
     * @returns {void}
     */
    public hideErrors(): void {
        this.props.orderStore.resetErrors();
    }

    /**
     *
     * @public
     * @returns {void}
     */
    public onPay(): void {
        this.props.orderStore.pay()
            .then(() => {
                alert('You order was successfully payed!');
                this.props.orderStore.reset();
                browserHistory.push(AppRoutesEnum.ORDER);
            });
    }

    /**
     *
     * @public
     * @returns {void}
     */
    public componentDidMount(): void {
        if (!this.props.orderStore.hasErrors()) {
            this.props.orderStore.create();
        }
    }

    /**
     * @public
     * @returns {JSX.Element}
     */
    public render(): JSX.Element {
        return (
            <div className="page checkout">
                <div className="container">
                    <h1 className="page-header">Checkout</h1>
                    {
                        this.props.orderStore.hasErrors() ?
                            <ErrorMessages
                                errors={this.props.orderStore.errors}
                                onClose={this.hideErrors}
                            /> : null
                    }
                    <Jumbotron>
                        <Row
                            name="orderForm"
                            noValidate
                            autoComplete="off"
                        >
                            <div className="order-items">
                                <h3>Products</h3>
                                <ProductsList list={this.props.orderStore.order.products} />
                            </div>
                            <div className="delivery">
                                <h3>Delivery</h3>
                                <Row>
                                    <Col xs={6}>
                                        <strong>Delivery Type:&nbsp;</strong>
                                    </Col>
                                    <Col xs={6} className="text-capitalize">
                                        {this.props.deliveryTypesStore.getBy('name', 'id', this.props.orderStore.order.deliveryType)}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={6}>
                                        <strong>Delivery Date:&nbsp;</strong>
                                    </Col>
                                    <Col xs={6}>
                                        {
                                            moment(this.props.orderStore.order.deliveryDate).format('MM/DD/YYYY H:mm')
                                        }
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={6}>
                                        <strong>Delivery Address:&nbsp;</strong>
                                    </Col>
                                    <Col xs={6}>
                                        {this.props.orderStore.order.deliveryAddress}
                                    </Col>
                                </Row>
                            </div>
                            <div className="contact-details">
                                <h3>Contact Details</h3>
                                <Row>
                                    <Col xs={6}>
                                        <strong>Name:&nbsp;</strong>
                                    </Col>
                                    <Col xs={6}>
                                        {this.props.orderStore.order.contactDetails.name}
                                    </Col>
                                </Row>
                                <Row>
                                    <Col xs={6}>
                                        <strong>Phone Number:&nbsp;</strong>
                                    </Col>
                                    <Col xs={6}>
                                        {this.props.orderStore.order.contactDetails.phoneNumber}
                                    </Col>
                                </Row>
                            </div>
                            <hr style={{borderColor: 'grey'}}/>
                            <div>
                                <p>
                                    Products: { this.props.orderStore.productsAmount }
                                </p>
                                <p>
                                    Delivery: { this.props.deliveryTypesStore.getPrice(this.props.orderStore.order.deliveryType) }
                                </p>
                                <p>
                                    <strong>Total: </strong>{ this.props.orderStore.order.totalAmount }
                                </p>
                            </div>
                            <div>
                                {
                                    this.props.orderStore.hasErrors() ?
                                        <Button
                                            className="pull-left"
                                            bsSize="large"
                                            bsStyle="primary"
                                        >
                                            <Link
                                                to="/"
                                                style={{textDecoration: 'none', color: 'white'}}
                                            >
                                                Edit
                                            </Link>
                                        </Button> : null
                                }
                                <Button
                                    className="pull-right"
                                    type="submit"
                                    bsSize="large"
                                    bsStyle="primary"
                                    disabled={this.props.orderStore.hasErrors()}
                                    onClick={this.onPay}
                                >
                                    Pay
                                </Button>
                            </div>
                        </Row>
                    </Jumbotron>
                </div>
            </div>
        );
    }

}

/**
 *
 * @param props
 * @returns {JSX.Element[]}
 * @constructor
 */
const ProductsList = props => {
    return (
        <div className="products-list">
            {
                props.list.map((item, i) => {
                    return (
                        <Row key={i}>
                            <Col xs={6} className="product-item-name">{item.name}</Col>
                            <Col xs={6} className="product-item-price">$ {item.price} x {item.count} </Col>
                        </Row>
                    );
                })
            }
        </div>
    );
};

module.exports = exports.default;