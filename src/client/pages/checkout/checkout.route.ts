/// <reference path="../../../../node_modules/@types/webpack-env/index.d.ts" />
import { browserHistory } from 'react-router';

import AppRoutesEnum  from '../../utils/AppRoutes.enum';
import { orderStore } from '../../stores';

export default {
    path: AppRoutesEnum.CHECKOUT,
    onEnter() {
        if (!orderStore.order.totalAmount) {
            browserHistory.push(AppRoutesEnum.ORDER);
        }
    },
    getComponent(location, cb) {
        require.ensure([], require => {
            cb(null, require('./checkout.component.tsx'));
        }, 'checkout');
    }
};

if (module.hot) {
    module.hot.accept();
}
module.exports = exports.default;