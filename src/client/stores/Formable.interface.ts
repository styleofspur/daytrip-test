interface Formable {
    errors:      Array<string>;
    hasErrors(): boolean;
    validate():  Array<string>|null;
}

export default Formable;