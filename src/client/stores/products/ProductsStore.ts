import { observable, action, computed } from 'mobx';

import Product       from '../Product.interface';
import Fetchable     from '../Fetchable.interface';
import RequestHelper from '../../helpers/RequestHelper';
import ProductTypes  from '../../utils/ProductTypes.enum';

class ProductsStore implements Fetchable {

    /**
     * @type {string}
     * @private
     */
    private _url: string = 'products';

    /**
     * @computed
     * @public
     * @returns {Product[]}
     */
    @computed
    public get pizzas(): Product[] {
        return this.list.filter(item => item.type === ProductTypes.PIZZA);
    }

    /**
     * @computed
     * @public
     * @returns {Product[]}
     */
    @computed
    public get beverages(): Product[] {
        return this.list.filter(item => item.type === ProductTypes.BEVERAGE)
    }

    /**
     * @public
     * @type {any}
     */
    @observable
    public list: Product[] = [];

    @observable
    public flags = {
        isFetched: false
    };

    /**
     *
     * @public
     * @returns {Promise<any>}
     */
    public init(): Promise<any> {
        return this.fetch();
    }

    /**
     *
     * @action
     * @public
     * @returns {Promise<any>}
     */
    @action
    public fetch(): Promise<any> {
        return RequestHelper.get(this._url)
            .then(list => {
                this.list = list;
                this.flags.isFetched = true;
            });
    }

    /**
     *
     * @public
     * @param {string} id
     * @returns {any}
     */
    public getPrice(id: string = ''): number {
        const item = this.list.find(i => i.id === id);
        return item ? item.price : 0;
    }

    /**
     *
     * @param {string} get
     * @param {string} field
     * @param {any} value
     * @returns {any|null}
     */
    public getBy(get: string = '', field: string = '', value: any): any|null {
        const item = this.list.find(i => i[field] === value);
        return item ? item[get] : null;
    }

}

const productsStore: ProductsStore = new ProductsStore();

export default productsStore;
export { ProductsStore };