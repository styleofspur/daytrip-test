interface DeliveryType {
    id:    string,
    name:  string,
    price: number
}

export default DeliveryType;