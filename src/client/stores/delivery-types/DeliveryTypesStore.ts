import { observable, action } from 'mobx';

import DeliveryType      from '../DeliveryType.interface';
import Fetchable         from '../Fetchable.interface';
import RequestHelper     from '../../helpers/RequestHelper';
import DeliveryTypesEnum from '../../utils/DeliveryTypes.enum';

class DeliveryTypesStore implements Fetchable {

    /**
     * @type {string}
     * @private
     */
    private _url: string = 'delivery-types';

    /**
     * @observable
     * @public
     * @type {DeliveryType[]}
     */
    @observable
    public list: DeliveryType[] = [];

    /**
     * @observable
     * @public
     * @type {{isFetched: boolean}}
     */
    @observable
    public flags = {
        isFetched: false
    };

    /**
     *
     * @public
     * @returns {Promise<any>}
     */
    public init(): Promise<any> {
        return this.fetch();
    }

    /**
     *
     * @action
     * @public
     * @returns {Promise<any>}
     */
    @action
    public fetch(): Promise<any> {
        return RequestHelper.get(this._url)
            .then(list => {
                this.list = list;
                this.flags.isFetched = true;
            });
    }

    /**
     *
     * @param id
     * @returns {boolean}
     */
    public isExpress(id: string = ''): boolean {
        const item = this.list.find(i => i.id === id);
        return item ? item.name === DeliveryTypesEnum.EXPRESS.toString() : false;
    }

    /**
     *
     * @param {string} id
     * @returns {number}
     */
    public getPrice(id: string = ''): number {
        const item = this.list.find(i => i.id === id);
        return item ? item.price : 0;
    }

    /**
     *
     * @public
     * @param {string} get
     * @param {string} field
     * @param {any} value
     * @returns {any|null}
     */
    public getBy(get: string = '', field: string = '', value: any): any|null {
        const item = this.list.find(i => i[field] === value);
        return item ? item[get] : null;
    }

}

const deliveryTypesStore: DeliveryTypesStore = new DeliveryTypesStore();

export default deliveryTypesStore;
export { DeliveryTypesStore };