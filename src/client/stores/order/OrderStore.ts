import { observable, action, computed } from 'mobx';
import * as moment                      from 'moment';

import Formable    from '../Formable.interface'
import Order       from '../Order.interface'

import RequestHelper      from '../../helpers/RequestHelper';
import UrlHelper          from '../../helpers/UrlHelper';
import DeliveryTypesEnum  from '../../utils/DeliveryTypes.enum';
import productsStore      from '../products/ProductsStore';
import deliveryTypesStore from '../delivery-types/DeliveryTypesStore';

class OrderStore implements Formable {

    /**
     * @type {string}
     * @private
     */
    private _url: string = 'orders';

    /**
     * @type {Order}
     * @private
     */
    private _defaultOrderData: Order = {
        products:        [],
        totalAmount:     0,
        deliveryType:    '',
        deliveryAddress: '',
        deliveryDate:    new Date(),
        contactDetails:  {
            name:        '',
            phoneNumber: ''
        },
        isPayed:         false
    };

    /**
     *
     * @computed
     * @public
     * @returns {number}
     */
    @computed
    public get productsAmount(): number {
        return !!this.order.products.length ? +((
            (this.order.totalAmount * 10 - deliveryTypesStore.getPrice(this.order.deliveryType) * 10) / 10
        ).toFixed(2)) : 0;
    }

    /**
     * @public
     * @type {Order|null}
     */
    @observable
    public order: Order = null;

    /**
     * @public
     * @type {string[]}
     */
    @observable
    public errors: string[] = [];

    /**
     *
     * @returns {void}
     * @private
     */
    private _fillFromQueryString(): void {
        const urlParams: any = UrlHelper.getQueryStringParams(window.location.href);

        this.order = {
            products:        [].concat(urlParams['products[]']),
            deliveryType:    deliveryTypesStore.getBy('id', 'name', urlParams.deliveryType.toLowerCase() || DeliveryTypesEnum.PICKUP),
            deliveryAddress: urlParams.deliveryType === DeliveryTypesEnum.PICKUP ? '' : urlParams.deliveryAddress,
            deliveryDate:    new Date(urlParams.deliveryDate),
            contactDetails:  {
                name:        urlParams['contactDetails.name'],
                phoneNumber: urlParams['contactDetails.phoneNumber']
            },
            totalAmount:     0,
            isPayed:         false
        };

        if (urlParams['products[]']) {
            this.order.products.forEach((product, i) => {
                const parts = product['split'](':');
                const id    = productsStore.getBy('id', 'name', parts[0]);

                this.order.products[i] = {
                    id,
                    name:  parts[0],
                    price: productsStore.getPrice(id),
                    type:  productsStore.getBy('id', 'price', id),
                    count: parts[1] || 0
                };
            });

        }

        this.updateTotalAmount();
    }

    /**
     *
     * @constructor
     * @public
     */
    public constructor() {
        this.order = this._defaultOrderData;
    }

    /**
     *
     * @public
     * @returns void
     */
    @action
    public validate(): string[] {
        if (!this.order.totalAmount) {
            this.errors.push('Add at least 1 product');
        }

        if (!this.order.deliveryType) {
            this.errors.push('Select delivery type');
        }

        if (deliveryTypesStore.isExpress(this.order.deliveryType) && !this.order.deliveryAddress) {
            this.errors.push('Select delivery address');
        }

        if (!this.order.deliveryDate) {
            this.errors.push('Delivery date is required');
        } else if (moment(this.order.deliveryDate).isBefore(moment(Date.now(), 'MM/DD/YYYY H:mm'))) {
            this.errors.push('Delivery date is invalid');
        }

        if (!this.order.contactDetails.name) {
            this.errors.push('Set your name');
        }

        if (!this.order.contactDetails.phoneNumber) {
            this.errors.push('Set your phone number');
        }

        return this.errors;
    }

    /**
     *
     * @action
     * @public
     * @returns {void}
     */
    @action
    public resetErrors(): void {
        this.errors = [];
    }

    /**
     *
     * @public
     * @action
     * @param list
     * @returns {void}
     */
    @action
    public addProducts(list: Array<any> = []): void {
        this.order.products = list;
        this.updateTotalAmount();
    }

    /**
     *
     * @action
     * @public
     * @returns {Promise<any>}
     */
    @action
    public create(): Promise<any> {
        const data = {
            products:        this.order.products.map(product => {
                return {id: product.id, count: product.count};
            }),
            deliveryType:    this.order.deliveryType,
            deliveryDate:    this.order.deliveryDate,
            deliveryAddress: this.order.deliveryAddress,
            contactDetails:  this.order.contactDetails,
            totalAmount:     this.order.totalAmount
        };

        return RequestHelper.post(this._url, data).then(order => this.order.id = order.id);
    }

    /**
     *
     * @action
     * @public
     * @returns {Promise<any>}
     */
    @action
    public pay(): Promise<any> {
        return RequestHelper.post(`${this._url}/${this.order.id}`)
            .then(() => this.order.isPayed = true);
    }

    /**
     * 
     * @public
     * @action
     * @returns {void}
     */
    @action 
    reset(): void {
        this.order = this._defaultOrderData;
    }

    /**
     *
     * @public
     * @returns {Promise<any>}
     */
    public init(): Promise<any> {
        return new Promise(resolve => {
            if (window.location.search) {
                this._fillFromQueryString();
                this.validate();
            }
            resolve();
        });
    }

    /**
     *
     * @public
     * @returns {boolean}
     */
    public hasErrors(): boolean {
        return !!this.errors.length;
    }

    /**
     *
     * @public
     * @returns {void}
     */
    public updateTotalAmount(): void {
        let val = 0;

        val += deliveryTypesStore.getPrice(this.order.deliveryType) * 10;
        if (!!this.order.products.length) {
            this.order.products.forEach(product => {
                val += (product.price * 10) * product.count;
            });
        }
        val = +((val / 10).toFixed(2));

        this.order.totalAmount = val;
    }

}

const userStore: OrderStore = new OrderStore();

export default userStore;
export { OrderStore };