interface Fetchable {
    list:    Array<any>,
    flags:   Object,
    init():  Promise<any>;
    fetch(): Promise<any>
}

export default Fetchable;