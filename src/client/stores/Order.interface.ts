import Product from './Product.interface';

interface Order {

    id?:             string,
    products:        Product[],
    deliveryType:    string,
    deliveryAddress: string,
    deliveryDate:    Date,
    totalAmount:     number,
    contactDetails:  {
        name:        string,
        phoneNumber: string
    },
    isPayed:         boolean

}

export default Order;