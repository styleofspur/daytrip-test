import ProductTypesEnum from '../utils/ProductTypes.enum';

interface Product {
    id:     string,
    name:   string,
    price:  number,
    type:   ProductTypesEnum,
    count?: number
}

export default Product;