/**
 * Represents a list of defined application delivery types.
 */
enum DeliveryTypes {
    PICKUP  = <any>'pickup',
    EXPRESS = <any>'express'
}

export default DeliveryTypes;