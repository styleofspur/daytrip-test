/**
 * Represents a list of defined application routes.
 */
enum AppRoutesEnum {

    ORDER    = <any>'/',
    CHECKOUT = <any>'/checkout'
}

export default AppRoutesEnum;