/**
 * Represents a list of defined application products.
 */
enum ProductTypes {
    PIZZA    = <any>'pizza',
    BEVERAGE = <any>'beverage'
}

export default ProductTypes;