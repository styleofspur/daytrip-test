import * as React    from 'react';
import * as ReactDOM from 'react-dom';
import { Provider }  from 'mobx-react';
import { Router, match, browserHistory }     from 'react-router';
import { RouterStore, syncHistoryWithStore } from 'mobx-react-router';

import RequestHelper from './helpers/RequestHelper';
import ConfigHelper  from './helpers/ConfigHelper';
import routes        from './routes';
import * as stores   from './stores';
import './style.scss';

const location                  = window.location.pathname;
const routingStore: RouterStore = new RouterStore();
const history                   = syncHistoryWithStore(browserHistory, routingStore);
const store = Object.assign({}, stores, {
    routingStore
});

const renderApp = (store, history, key): void => {
    ReactDOM.render(
        <Provider {...store}>
            <Router history={history} routes={routes} key={key} />
        </Provider>,
        document.getElementById('app')
    );
};

const hmr = () => {
    if (module.hot) {
        module.hot.accept(['./index.tsx', './routes'], () => {
            renderApp(store, history, Math.random());
        });
    }
};

Promise.resolve()
    .then(() => {
        ConfigHelper.init();
        RequestHelper.init();

        return Promise.all([
            stores.productsStore.init(),
            stores.deliveryTypesStore.init()
        ])
    })
    .then(() => stores.orderStore.init())
    .then(() => {
        match({ routes, location}, () => renderApp(store, history, Math.random()));
        hmr();
    }).catch(console.error);


