import * as selectn from 'selectn';

export default class ConfigHelper {

    /**
     * @static
     * @type {{}}
     * @private
     */
    private static _config = {};

    /**
     *
     * @public
     * @static
     * @returns {void}
     */
    public static init(): void {
        ConfigHelper._config = require('../../../config.json');
    }

    /**
     *
     * @public
     * @static
     * @param {string} path
     * @returns {any}
     */
    public static get(path: string = ''): any {
        if (!Object.keys(ConfigHelper).length) ConfigHelper.init();
        return selectn(path, ConfigHelper._config);
    }
}