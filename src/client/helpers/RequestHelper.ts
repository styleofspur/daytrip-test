import * as superagent          from 'superagent';
import * as superagentJsonapify from 'superagent-jsonapify';

import ConfigHelper from './ConfigHelper';

export default class RequestHelper {

    /**
     *
     * @static
     * @param {string} path
     * @returns {string}
     * @private
     */
    private static _combineUrl(path: string = ''): string {
        return `${ConfigHelper.get('api.url')}/${path}`;
    }

    /**
     *
     * @public
     * @static
     * @returns @void
     */
    public static init(): void {
        superagentJsonapify(superagent);
    }

    /**
     *
     * @public
     * @static
     * @param {string} url
     * @param {string} query
     * @returns {any}
     */
    public static get(url, query = ''): any {
        return superagent.get(`${RequestHelper._combineUrl(url)}${query ? '?' + query : ''}`)
            .then(response => response.body)
            .catch(console.error);
    }

    /**
     *
     * @public
     * @static
     * @param url
     * @param data
     * @returns {any}
     */
    public static post(url, data = {}): any {
        return superagent.post(RequestHelper._combineUrl(url))
            .send(data)
            .then(response => response.body);
    }
}