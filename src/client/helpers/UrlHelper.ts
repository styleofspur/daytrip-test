export default class URLHelper {

    /**
     *
     * @public
     * @static
     * @param {string} url
     * @returns {string}
     */
    public static getUrlQueryString(url: string = ''): string {
        let parts = url ? url.split('?') : undefined;
        return parts && parts[1] ? parts[1] : undefined;
    }

    /**
     *
     * @public
     * @static
     * @param {string} url
     * @returns {{}}
     */
    static getQueryStringParams(url: string = ''): any {

        const query = this.getUrlQueryString(url);
        if(!query) return;

        const queryString = {};
        const consts = query.split('&');
        for (let i = 0; i < consts.length; i++) {
            const pair = consts[i].split('=');
            // If first entry with this name
            if (queryString[pair[0]] === undefined) {
                queryString[pair[0]] = decodeURIComponent(pair[1]);
                // If second entry with this name
            } else if (typeof queryString[pair[0]] === 'string') {
                const arr = [queryString[pair[0]], decodeURIComponent(pair[1])];
                queryString[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                queryString[pair[0]].push(decodeURIComponent(pair[1]));
            }
        }
        return queryString;
    }

    /**
     *
     * @public
     * @static
     * @param {string} url
     * @param {string} key
     * @returns {string|undefined}
     */
    static getQueryStringParam(url: string = '', key: string = ''): string|undefined {
        let urlParams = this.getQueryStringParams(url);
        return urlParams ? urlParams[key] : undefined;
    }
}
