import * as React from 'react';
import {
    Alert
} from 'react-bootstrap';

export class ErrorMessages
    extends React.Component<{errors: Array<any>, onClose: Function}, {}>
{

    /**
     * @public
     * @static
     * @type {*}
     */
    public static defaultProps = {
        errors:  [],
        onClose: () => {}
    };

    /**
     *
     * @public
     * @returns {JSX.Element}
     */
    public render(): JSX.Element {
        return (
            <Alert bsStyle="danger" onDismiss={this.props.onClose}>
                {
                    this.props.errors.map((error, i) => {
                        return <p key={i}>{error}</p>
                    })
                }
            </Alert>
        )
    }
}