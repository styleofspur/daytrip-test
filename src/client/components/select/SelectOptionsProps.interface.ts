import SelectOptionsSettings from './SelectOptionsSettings.interface';

interface SelectOptionsProps {
    list:             Array<any>,
    name:             string,
    label:            string,
    emptyOptionText?: string,
    defaultValue:     string,
    onChange?:        React.EventHandler<any>,
    options?:         SelectOptionsSettings
}

export default SelectOptionsProps;