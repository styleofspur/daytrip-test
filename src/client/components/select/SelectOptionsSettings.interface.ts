interface SelectOptionSettings {
    cssClass?:    string,
    valueProp?:   string,
    displayProp?: string,
    required?:    boolean,
    disabled?:    boolean,
}

export default SelectOptionSettings;