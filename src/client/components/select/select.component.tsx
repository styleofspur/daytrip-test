import * as React from 'react';
import {
    FormGroup,
    ControlLabel,
    FormControl
} from 'react-bootstrap';

import SelectOptionProps from './SelectOptionsProps.interface';


export class Select extends React.Component<SelectOptionProps, {}> {

    /**
     *
     * @public
     * @returns {Array<JSX.Element>}
     */
    public getOptions(): Array<JSX.Element> {
        return this.props.list.map((item, i) => {
            return (
                <option
                    key={i}
                    value={item[this.props.options.valueProp] || item}
                    className={this.props.options.cssClass || ''}
                >
                    { item[this.props.options.displayProp] || item}
                </option>
            );
        });
    }

    /**
     *
     * @public
     * @returns {JSX.Element}
     */
    public render(): JSX.Element {
        return (
            <FormGroup bsSize="large" className={this.props.options.required ? 'required' : ''}>
                <ControlLabel>{this.props.label}</ControlLabel>
                <FormControl
                    componentClass="select"
                    name={this.props.name}
                    onChange={this.props.onChange}
                    disabled={this.props.options.disabled}
                    defaultValue={this.props.defaultValue}
                >
                    {
                        this.props.emptyOptionText ?
                            <option value="">{this.props.emptyOptionText}</option> : null
                    }
                    { this.getOptions() }
                </FormControl>
            </FormGroup>
        );
    }

}
