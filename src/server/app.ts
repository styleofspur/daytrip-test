import * as express    from 'express';
import * as http       from 'http';
import * as bodyParser from 'body-parser';
import * as cors       from 'cors';

import config      from './config';
import MongoHelper from './helpers/mongo';

import OrderController        from './controllers/OrderController';
import ProductController      from './controllers/ProductController';
import DeliveryTypeController from './controllers/DeliveryTypeController';

const app = express();
console.log('App starting...');

MongoHelper.init().then(() =>{
    console.log('Database initialized');

    // Create HTTP server
    const server = http.createServer(app);
    const port   = config.get('http:port');

    // add middlewares
    app.use(cors());
    app.use(bodyParser.json());

    // setup routing
    app.get('/products',       ProductController.getAll);
    app.get('/delivery-types', DeliveryTypeController.getAll);
    app.post('/orders',        OrderController.create);
    app.post('/orders/:id',    OrderController.pay);

    // Start the HTTP server
    server.listen(port, () => {
        console.log('HTTP Server started at port ' + server.address().port);
    });
}).catch(console.error);