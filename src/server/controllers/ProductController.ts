import { ProductModel } from '../db/models';

export default class ProductController {

    /**
     *
     * @public
     * @static
     * @param req
     * @param res
     * @param next
     * @returns void
     */
    public static getAll(req, res, next): void {

        ProductModel.find()
            .then(products => res.json(products))
            .catch(err => {
                console.log(err);
                res.json(err);
            });
    }

}