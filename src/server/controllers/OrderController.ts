import { OrderModel } from '../db/models';

export default class OrderController {

    /**
     *
     * @public
     * @static
     * @param req
     * @param res
     * @returns void
     */
    public static create(req, res): void {
        const order = new OrderModel(req.body);
        order.save()
            .then(() => res.json(order))
            .catch(err => {
                console.error(err);
                res.status(500);
                res.json(err);
            });
    }

    /**
     *
     * @public
     * @static
     * @param req
     * @param res
     */
    public static pay(req, res): void {
        OrderModel.findById(req.params.id)
            .then(order => {
                order.isPayed = true;
                return order.save();
            })
            .then(order => res.json(order))
            .catch(err => {
                console.error(err);
                res.status(500);
                res.json(err);
            });
    }

}