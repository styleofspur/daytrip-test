import { DeliveryTypeModel } from '../db/models';

export default class DeliveryTypeController {

    /**
     *
     * @public
     * @static
     * @param req
     * @param res
     * @param next
     * @returns void
     */
    public static getAll(req, res, next): void {

        DeliveryTypeModel.find()
            .then(products => res.json(products))
            .catch(err => {
                console.log(err);
                res.json(err);
            });
    }

}