const mongoose = require('mongoose');

import MongoConnection        from '../utils/mongo';
import { init as initModels } from '../db/models';

export default class MongoHelper {

    /**
     * @public
     * @static
     * @type {string}
     */
    public static CONNECTION_ID = 'DaytripTest';

    /**
     * @static
     * @type {{}}
     * @private
     */
    private static _connections = {};

    /**
     *
     * @public
     * @static
     * @param {string} connectionId
     * @param {*} options
     * @returns {Promise<any>}
     */
    public static init(connectionId: string = MongoHelper.CONNECTION_ID, options = {}): Promise<any> {
        mongoose['Promise'] = global.Promise;
        return MongoHelper.initDbConnection(connectionId, options);
    }

    /**
     *
     * @public
     * @static
     * @param {string} connectionId
     * @param {*} options
     * @returns {Promise<any>}
     */
    public static initDbConnection(connectionId: string = MongoHelper.CONNECTION_ID, options = {}): Promise<any> {

        if(MongoHelper._connections[connectionId]) return Promise.resolve();

        let connection = new MongoConnection(connectionId);
        MongoHelper._connections[connectionId] = connection;

        return connection.connect(options).then(MongoHelper.initModels);
    }

    /**
     *
     * @public
     * @static
     * @returns void
     */
    public static initModels(): void {
        initModels(MongoHelper._connections[MongoHelper.CONNECTION_ID]);
    }

    /**
     *
     * @public
     * @static
     * @param {string} connectionId
     * @returns {MongoConnection}
     */
    public static getDbConnection(connectionId: string = MongoHelper.CONNECTION_ID): MongoConnection {
        return MongoHelper._connections[connectionId];
    }

}  