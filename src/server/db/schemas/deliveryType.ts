import * as mongoose from 'mongoose';

export default new mongoose.Schema({
    name: {
        type:    String,
        enum:    ['pickup', 'express'],
        require: true,
        default: 'pickup'
    },
    price: {
        type:     Number,
        required: true,
    },
});
