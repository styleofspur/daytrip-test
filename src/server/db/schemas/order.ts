import * as mongoose from 'mongoose';

export default new mongoose.Schema({
    products: [
        {
            product: {
                type:    mongoose.Schema.Types.ObjectId,
                ref:     'product',
                require: true,
            },
            count: {
                type:    Number,
                require: true
            }
        }
    ],
    totalAmount: {
        type:     Number,
        required: true,
    },
    deliveryType: {
        type:    mongoose.Schema.Types.ObjectId,
        ref:     'deliveryType',
        require: true
    },
    deliveryAddress: {
        type: String,
    },
    deliveryDate: {
        type:     Date,
        required: true,
        default:  Date.now
    },
    isPayed: {
        type:     Boolean,
        required: true,
        default:  false
    },
    addedAt: {
        type: Date,
        default: Date.now
    }
});
