import * as mongoose from 'mongoose';

export default new mongoose.Schema({
    name: {
        type:     String,
        required: true,
        unique:   true
    },
    price: {
        type:     Number,
        required: true,
    },
    type: {
        type:    String,
        enum:    ['pizza', 'beverage'],
        require: true,
        default: 'pizza'
    }
});
