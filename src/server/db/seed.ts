import MongoHelper                         from '../helpers/mongo';
import { ProductModel, DeliveryTypeModel } from './models';
const generatePrice = (): number => +(Math.random() * 10).toFixed(2);
const products = [
    {
        name: 'Four Cheese',
        price: generatePrice(),
        type: 'pizza'
    },
    {
        name: 'Margherita',
        price: generatePrice(),
        type: 'pizza'
    },
    {
        name: 'Seafood',
        price: generatePrice(),
        type: 'pizza'
    },
    {
        name: 'Lemonade',
        price: generatePrice(),
        type: 'beverage'
    },
    {
        name: 'Pepsi',
        price: generatePrice(),
        type: 'beverage'
    }
];
const deliveryTypes = [
    {
        name: 'pickup',
        price: 0
    },
    {
        name:  'express',
        price: generatePrice()
    }
];

MongoHelper.init()
    .then(() => {
        console.log('Database initialized');

        const promises = [];
        products.forEach(product => promises.push((new ProductModel(product)).save()));

        return Promise.all(promises);
    })
    .then(() => {
        const promises = [];
        deliveryTypes.forEach(deliveryType => promises.push((new DeliveryTypeModel(deliveryType)).save()));

        return Promise.all(promises);
    })
    .then(() => {
        console.log('DB seed is done');
        process.exit(0);
    })
    .catch(console.error);