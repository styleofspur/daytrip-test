import * as mongoose from 'mongoose';

import OrderSchema       from './schemas/order';
import ProductSchema     from './schemas/product';
import DeliveryTypechema from './schemas/deliveryType';

// normalize JSON output
mongoose.plugin(require('meanie-mongoose-to-json'));

export let OrderModel;
export let ProductModel;
export let DeliveryTypeModel;

export const init = connection => {
    OrderModel        = connection.model('order',        OrderSchema);
    ProductModel      = connection.model('product',      ProductSchema);
    DeliveryTypeModel = connection.model('deliveryType', DeliveryTypechema);
};
