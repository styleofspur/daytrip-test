import MongoHelper                                     from '../helpers/mongo';
import { ProductModel, DeliveryTypeModel, OrderModel } from './models';

MongoHelper.init()
    .then(() => {
        console.log('Database initialized');
        return Promise.all([
            ProductModel.remove(),
            DeliveryTypeModel.remove()
        ]);
    })
    .then(() => OrderModel.remove())
    .then(() => {
        console.log('DB clean is done');
        process.exit(0);
    })
    .catch(console.error);