import * as mongoose from 'mongoose';

import config from '../config';

const makeMongoUrl: Function = require('mongo-make-url');

export default class MongoConnection {

    /**
     * @private
     * @type {string}
     */
    private _connectionId: string;

    /**
     * @private
     */
    private _config;

    /**
     * @private
     */
    private _connection;

    /**
     * @type {boolean}
     * @private
     */
    private _connected: boolean = false;

    /**
     *
     * @public
     * @constructor
     * @param connectionId
     */
    public constructor(connectionId: string = '') {
        this._connectionId = connectionId;

        this._config = config.get(`mongo:${this._connectionId}`);
        if(!this._config || !this._config.host || !this._config.db) throw new Error('Invalid config');

    }

    /**
     *
     * @public
     * @returns {string}
     */
    public toString(): string {
        return `MongoDB Connection #${this._connectionId}`;
    }

    /**
     *
     * @public
     * @param {*} options
     * @returns {any}
     */
    public makeUrl(options = {}): string {
        return makeMongoUrl(Object.assign({}, this._config, options));
    }

    /**
     *
     * @public
     * @param {*} options
     * @returns {Promise<any>}
     */
    public connect(options = {}): Promise<any> {
        if(this._connection) throw new Error('Already connected, please disconnect first');

        return new Promise((resolve, reject) => {

            this._connection = mongoose.createConnection(this.makeUrl(options));

            this._connection.on('opening', () => {
                console.log(`Database reconnecting... ${this._connection.readyState}`);
            });

            this._connection.on('open', () => {
                console.log(`${this} has been opened`);
                this._connected = true;
                resolve();
            });

            this._connection.on('error', error => {
                console.log(`${this} interrupt with an error`, error);

                if (this._connected) {
                    console.log(`Trying to reconnect to ${this}`);

                    this.close().then(() => {
                        this._connected = false;
                        this._connection = null;
                        this.connect(options);
                    });

                } else {
                    reject(error);
                }
            });
        });
    }

    /**
     *
     * @public
     * @returns {Promise<any>}
     */
    public close(): Promise<any> {
        if(!this._connection) throw new Error('Not connection found, please connect first');

        return new Promise((resolve, reject) => {
            return this._connection.close(err => {
                if(err) return reject(err);

                console.log(`${this} has been closed`);
                resolve();
            });
        });
    }

    /**
     *
     * @param {string} name
     * @param {mongoose.Schema} schema
     * @returns {mongoose.Model}
     */
    public model(name: string = '', schema: mongoose.Schema): mongoose.Model<any> {
        return this._connection.model(name, schema);
    };

}
