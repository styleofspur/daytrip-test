import Express              from 'express';
import webpack              from 'webpack';
import path                 from 'path';
import WriteFilePlugin      from 'write-file-webpack-plugin';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import { argv as yargs }    from 'yargs';

import webpackConfig from './webpack.config';
import BuildHelper   from './build/helper';

BuildHelper.cleanDist('./dist');

const options = Object.assign({}, yargs, {
    protocol: 'http://',
    host:     'localhost',
    port:     8080
});

webpackConfig.plugins = webpackConfig.plugins.concat([
    new WriteFilePlugin({
        log: false
    })
]);
webpackConfig.entry = webpackConfig.entry.concat([
    'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000',
]);

const compiler = webpack(webpackConfig);
const app      = new Express();

app.use(webpackDevMiddleware(compiler, {
    publicPath:   webpackConfig.output.publicPath,
    quiet:        false,
    watchOptions: {
        aggregateTimeout: 300,
        poll:             1000
    },
    historyApiFallback: true,
    cache:              true,
    colors:             true,
    host:               '127.0.0.1',
    stats: {
        colors:       true,
        errors:       true,
        warnings:     true,
        chunks:       false,
        version:      false,
        assets:       false,
        timings:      false,
        entrypoints:  false,
        chunkModules: false,
        chunkOrigins: false,
        cached:       false,
        cachedAssets: false,
        reasons:      false,
        usedExports:  false,
        children:     false,
        source:       false,
        modules:      false
    }
}));

app.use(webpackHotMiddleware(compiler));

app.get('*', (req, res) => res.sendFile(path.join(__dirname, 'dist', 'client', 'index.html')));

app.listen(options.port, options.host, err => {
    if (err) {
        return console.log(err);
    }

    console.log(`Listening at ${options.protocol}${options.host}:${options.port}/`);
});