import webpack           from 'webpack';
import path              from 'path';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { argv }          from 'yargs';

import BuildHelper from './build/helper';

const paths = {
    src:    path.resolve(__dirname, './src/client'),
    vendor: path.resolve(__dirname, './node_modules'),
    dist:   path.resolve(__dirname, './dist/client')
};

const baseConfig = {
    cache: true,
    watch: argv.watch,
};

const rules = {
    ts: {
        test: /\.ts(x?)$/,
        use: [
            { loader: 'react-hot-loader' },
            {
                loader: 'ts-loader' ,
                options: {
                    transpileOnly: true
                },
            },
        ],

        include: [
            /src\/client/
        ],
        exclude: [
            /src\/server/
        ]
    },
    styles: {
        test: /\.scss$/,
        use: [
            { loader: 'style-loader' },
            { loader: 'css-loader'   },
            { loader: 'sass-loader'  },
        ],
    },
    images: {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
            {
                loader: 'file-loader',
                query: {
                    hash: 'sha512',
                    digest: 'hex',
                    name: '[hash].[ext]'
                }
            },
            {
                loader: 'image-webpack-loader',
                query: {
                    bypassOnDebug:     true,
                    optimizationLevel: 7,
                    interlaced:        false
                }
            }
         ]
    },
    fonts: [
        {
            test: /\.woff(\?.*)?$/, use: 'url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/font-woff',
        }, {
            test: /\.woff2(\?.*)?$/, use: 'url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/font-woff2',
        }, {
            test: /\.otf(\?.*)?$/, use: 'file-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=font/opentype',
        }, {
            test: /\.ttf(\?.*)?$/, use: 'url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=application/octet-stream',
        }, {
            test: /\.eot(\?.*)?$/, use: 'file-loader?prefix=fonts/&name=[path][name].[ext]',
        }, {
            test: /\.svg(\?.*)?$/, use: 'url-loader?prefix=fonts/&name=[path][name].[ext]&limit=10000&mimetype=image/svg+xml',
        }, {
            test: /\.(png|jpg)$/, use: 'url-loader?limit=8192',
        }
    ]
};

const plugins = {
    commonChunks: () => {
        return new webpack.optimize.CommonsChunkPlugin({
            name:      'vendors',
            filename:  'vendors.[hash].js',
            minChunks: (module, count) => {
                return module.resource && module.resource.indexOf('node_modules') !== -1 && count >= 1;
            }
        });
    },
    html: (options) => {
        return new HtmlWebpackPlugin(options);
    },
    provide: () => {
        return new webpack.ProvidePlugin({
            React:    'react',
            ReactDOM: 'react-dom',
            moment:   'moment'
        });
    },
    sourceMap: (path) => {
        return new webpack.SourceMapDevToolPlugin({
            filename: '[file].map',
            append:   `\n//# sourceMappingURL=${path}[url]`
        })
    },
    hmr: () => {
        return new webpack.HotModuleReplacementPlugin();
    }
};

// resolves
const resolve = {
    modules: [
        paths.src,
        'node_modules'
    ],
    extensions: ['.js', '.ts', '.tsx', '.json', '.scss']
};

// config data per entry
const entryPoints = {
    context: paths.src,
    entry: [
        './index.tsx'
    ],
    resolve,
    module:  {
        rules: [
            rules.ts,
            rules.styles,
            rules.images,
           ...rules.fonts,
        ]
    },
    output:  {
        path:          paths.dist,
        publicPath:    '/',
        filename:      'app.[chunkhash].js',
        chunkFilename: '[name].[chunkhash].js'
    },
    plugins: [
        plugins.commonChunks(),
        plugins.provide(),
        plugins.sourceMap('/'),
        plugins.html({
            template: path.join(paths.src, 'index.html'),
            inject:   'body',
            filename: 'index.html'
        }),
        plugins.hmr()
    ]
};

BuildHelper.cleanDist('./dist');

export default Object.assign({}, entryPoints, baseConfig);
