# Daytrip Test Task

- [Requirements](#markdown-header-requirements)
- [Install](#markdown-header-install)
    - [Global packages](#markdown-header-global-packages)
    - [Local packages](#markdown-header-local-packages)
- [Seed and clean database](#markdown-header-seed-and-clean-database)
- [Usage](#markdown-header-usage)
    - [Run API Server](#markdown-header-run-api-server)
    - [Run development static server](#markdown-header-run-development-static-server)
- [URL params](#markdown-header-url-params)
- [TODO](#markdown-header-todo)

## Requirements

> Create a simple application based on TypeScript, React, React-Router, MobX and optionally MongoDB. 
> The application should have one shared global store (may be a routing store) and one store that is used by each page only. You are not required to do any more than basic styles, or just use plain HTML.
> 
> Imagine you have a pizza point. Create a pizza configurator with a few options (first page) and a checkout form (second page). 
> A customer should be able to view a preconfigured pizza based on page query parameters. Use page query to pass data from configurator to checkout page.

## Install
### Global packages

- Node 6.9.1
- MongoDB was installed with docker: 
```bash
    docker run --name daytrip-test-mongo -p 27017:27017 -d mongo
    docker start daytrip-test-mongo
```

### Local packages
`npm install`

## Seed and clean database

Run `npm run db:seed` in order to seed DB with test data.
Run `npm run db:clean` in order to clean DB.

## Usage

### Run API Server
Run `npm run server`. API is available on [http://localhost:8081](http://localhost:8081).

### Run development static server
Run `npm start`. App is available on [http://localhost:8080](http://localhost:8080).

## URL params
When you want to run the app using URL params you need to check the next rules. The list of available params are:

 - **deliveryType** (either *pickup* (default) or *express*).
 - **deliveryAddress** (any string value). REQUIRED if deliveryType is *express*, ignored if deliveryType is *pickup*.
 - **deliveryDate** (date string in the next format: MM/DD/YYYY H:mm, for example 12/12/2017 20:30). REQUIRED.
 - **contactDetails.name** (any string value). REQUIRED.
 - **contactDetails.phoneNumber** (any string value). REQUIRED.
 - **products[]**. REQUIRED.
        
    "[]" - means array
    
    Must be a string in format \<productName\>:\<count\>. 
    
    Default count is 1. For example: 
    
    *products[]=Four Cheese:2&products[]=Pepsi* 
    
    It will be parsed as 2 items of Four Cheeses pizzas and 1 Pepsi.
    
    Available products: 
    
    - Four Cheese
    - Margherita
    - Seafood
    - Lemonade
    - Pepsi
  
## TODO
1. add strict validation rules for phone number;
2. implement different API errors (404 for not found etc.);
3. fix moment.js warnings;
4. move DB logic on backend to repositories;
5. cover API, stores & components with unit tests;
6. cover app with E2E tests.