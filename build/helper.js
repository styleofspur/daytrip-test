import del from 'del';

export default class BuildHelper {

    /**
     *
     * @static
     * @returns {void}
     * @param {string|null} dir
     */
    static cleanDist(dir = null) {
        del.sync(dir, {force: true});
    }

}
